package com.jeremytech.utils.string;

/**
 * 字符串值相关操作工具实现
 *
 * @author jeremy
 * @date 2020-12-16
 * @copyright JeremyTech
 * @since 1.0.0
 */
public class StringValueUtils {

    /**
     * 判断指定的字符串是否有值
     *
     * @param s 指定字符串
     * @return null或者空字符串返回false，其他返回true
     */
    public static final boolean isBlank(CharSequence s) {
        return (s == null || s.length() == 0);
    }

    /**
     * 判断一组字符串中是否所有的字符串都没有值
     *
     * @param sequences 待判断的字符串组
     * @return 全部字符串都没有值返回true，其他返回false
     */
    public static final boolean isAllBlank(CharSequence... sequences) {
        for (CharSequence s : sequences) {
            if (!isBlank(s)) return false;
        }
        return true;
    }

    /**
     * 判断一组参数中是否包含空（null或者空字符串）的字符串
     *
     * @param sequences 待判断的字符串组
     * @return 参数中包含null或者空字符串返回true，其他返回false
     */
    public static final boolean isAnyBlank(CharSequence... sequences) {
        for (CharSequence s : sequences) {
            if (isBlank(s)) return true;
        }
        return false;
    }

    /**
     * 获取一组字符参数中包含有效值（非空并且字符串长度大于0）的参数个数
     *
     * @param sequences 待判断的字符串
     * @return 非空并且字符串长度大于0的参数个数
     */
    public static final int countNotBlank(CharSequence... sequences) {
        int count = 0;
        for (CharSequence s : sequences) {
            if (!isBlank(s)) count++;
        }
        return count;
    }
}

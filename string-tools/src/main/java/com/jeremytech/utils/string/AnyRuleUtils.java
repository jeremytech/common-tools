package com.jeremytech.utils.string;

import java.util.regex.Pattern;

/**
 * 字符串各种常用规则判定工具
 *
 * @author jeremy
 * @date 2021-01-11
 * @copyright JeremyTech
 * @since 1.0.0
 */
public class AnyRuleUtils {

    /**
     * 火车车次
     */
    public static final Pattern PATTERN_RAILWAY_NUMBER = Pattern.compile("^[GCDZTSPKXLY1-9]\\d{1,4}$");
    /**
     * 手机机身码IMEI
     */
    public static final Pattern PATTERN_IMEI = Pattern.compile("^\\d{15,17}$");
    /**
     * 带IP和端口的HTTP/HTTPS/FTP/FTPS 协议的URI
     */
    public static final Pattern PATTERN_NETWORK_URI = Pattern.compile("^((ht|f)tp[s]?:\\/\\/)?[\\w-]+(\\.[\\w-]+)+:\\d{1,5}\\/?$");
    /**
     * 手机号码（中国）
     */
    public static final Pattern PATTERN_PHONE = Pattern.compile("^(?:(?:\\+|00)86)?1(?:(?:3[\\d])|(?:4[5-7|9])|(?:5[0-3|5-9])|(?:6[5-7])|(?:7[0-8])|(?:8[\\d])|(?:9[1|8|9]))\\d{8}$");

    /**
     * 判断输入的字符串是否火车车次
     *
     * @param railwayNumber 待验证的字符串
     * @return
     */
    public static final boolean isRailwayNumber(String railwayNumber) {
        return isMatched(PATTERN_RAILWAY_NUMBER, railwayNumber);
    }

    /**
     * 判断输入的字符串是否手机机身码IMEI
     *
     * @param imei 待验证的字符串
     * @return
     */
    public static final boolean isImei(String imei) {
        return isMatched(PATTERN_IMEI, imei);
    }

    /**
     * 判断输入的字符串是否为HTTP、HTTPS、FTP、FTPS协议的完整URI，包含IP和端口
     *
     * @param uri 待验证的字符串
     * @return
     */
    public static final boolean isNetWorkUri(String uri) {
        return isMatched(PATTERN_NETWORK_URI, uri);
    }

    /**
     * 判断输入的字符串是否为中国大陆的手机号码，+86、0086开头的也满足判定
     *
     * @param phone 待验证的字符串
     * @return
     */
    public static final boolean isPhone(String phone) {
        return isMatched(PATTERN_PHONE, phone);
    }

    /**
     * 判断输入的字符串是否满足给定的正则匹配
     *
     * @param pattern 期望的正则
     * @param input   待验证的字符串
     * @return
     */
    public static final boolean isMatched(Pattern pattern, String input) {
        if (pattern == null) return false;
        if (StringValueUtils.isBlank(input)) return false;
        return pattern.matcher(input).matches();
    }
}

package com.jeremytech.test.utils.string;

import com.jeremytech.utils.string.AnyRuleUtils;
import org.junit.Assert;
import org.junit.Test;

public class AnyRuleUtilsTest {

    @Test
    public void isRailwayNumber() {
        boolean ret = AnyRuleUtils.isRailwayNumber("D5050");
        Assert.assertTrue("D5050不是火车车次，判断错误", ret);

        ret = AnyRuleUtils.isRailwayNumber("G308");
        Assert.assertTrue("G308不是火车车次，判断错误", ret);

        ret = AnyRuleUtils.isRailwayNumber("M797");
        Assert.assertFalse("M797是火车车次，判断错误", ret);
    }

    @Test
    public void isNetworkUri() {
        boolean ret = AnyRuleUtils.isNetWorkUri("http://192.168.1.0:8080");
        Assert.assertTrue(ret);

        ret = AnyRuleUtils.isNetWorkUri("https://192.168.1.0:8080");
        Assert.assertTrue(ret);

        ret = AnyRuleUtils.isNetWorkUri("ftp://192.168.1.0:8080");
        Assert.assertTrue(ret);

        ret = AnyRuleUtils.isNetWorkUri("ftps://192.168.1.0:8080");
        Assert.assertTrue(ret);

        ret = AnyRuleUtils.isNetWorkUri("file://192.168.1.0:8080");
        Assert.assertFalse(ret);

        ret = AnyRuleUtils.isNetWorkUri("ws://192.168.1.0");
        Assert.assertFalse(ret);
    }

    @Test
    public void isPhone() {
        boolean ret = AnyRuleUtils.isPhone("8618311201212");
        Assert.assertFalse(ret);

        ret = AnyRuleUtils.isPhone("+8618311201212");
        Assert.assertTrue(ret);

        ret = AnyRuleUtils.isPhone("008618311201212");
        Assert.assertTrue(ret);

        ret = AnyRuleUtils.isPhone("18311201212");
        Assert.assertTrue(ret);
    }
}
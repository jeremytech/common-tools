package com.jeremytech.utils.tree.modle;

import com.jeremytech.utils.string.StringValueUtils;
import lombok.Getter;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * 节点模型
 */
@Getter
public class TreeNode implements Serializable {

    public TreeNode(String id, String parentId, int level, Set<TreeNode> children) {
        if (StringValueUtils.isBlank(id)) throw new IllegalArgumentException("id must be not null nor empty");
        if (StringValueUtils.isBlank(parentId))
            throw new IllegalArgumentException("parentId must be not null nor empty");

        this.id = id;
        this.parentId = parentId;
        this.level = level;
        this.children = children;
    }

    public TreeNode(String id, String parentId, int level) {
        this(id, parentId, level, new HashSet<>());
    }

    private String id;
    private String parentId;
    private int level;
    private Set<TreeNode> children;

    /**
     * 添加一个子节点
     *
     * @param child 待添加的子节点
     * @return 待添加子节点的父节点，即函数调用者本身
     */
    public TreeNode addChild(TreeNode child) {
        if (child == null) throw new IllegalArgumentException("child must not be null");
        if (this.children == null) this.children = new HashSet<>();

        child.parentId = this.id;
        child.level = this.level + 1;
        this.children.add(child);
        return this;
    }

    /**
     * 添加多个子节点
     *
     * @param children 待添加的子节点集合
     * @param append   是否追加
     * @return 待添加子节点的父节点，即函数调用者本身
     */
    public TreeNode addChildren(Collection<TreeNode> children, boolean append) {
        if (children == null) throw new IllegalArgumentException("children must not be null");
        if (this.children == null) this.children = new HashSet<>();

        if (!append) this.children.clear();

        children.forEach(child -> addChild(child));
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TreeNode treeNode = (TreeNode) o;
        return this.id == treeNode.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

package com.jeremytech.utils.tree;

import com.jeremytech.utils.string.StringValueUtils;
import com.jeremytech.utils.tree.modle.TreeNode;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 树结构数据操作工具接口
 *
 * @author jeremy
 * @date 2020-12-17
 * @copyright JeremyTech
 * @since 1.0.0
 */
public class TreeUtils {

    private static String TMP_ROOT_ID = "root";
    private static String TMP_ROOT_PARENT = "N/A";

    /**
     * 将一组节点根据父节点ID添加到父节点的子节点集合中，并返回父节点对象。
     * <p>
     * <b>注意：</b>
     * <li>调用此方法后参数nodes的children和level会被重置</li>
     * <li>如果参数nodes中的节点存在不能追溯到root的路径，则返回的root节点也不能向下获取到这些节点</li>
     * </p>
     *
     * @param nodes  包含了root节点的节点集合
     * @param rootId root节点的ID
     * @return root节点
     * @throws IllegalArgumentException nodes必须不为null
     * @throws IllegalArgumentException nodes中必须包含id为rootId的根节点
     */
    public static TreeNode buildTreeWithRoot(Collection<TreeNode> nodes, String rootId) {
        if (nodes == null) throw new IllegalArgumentException("nodes list must be not null");

        Map<String, TreeNode> idMappedNode = nodes.stream()
                .collect(Collectors.toMap(TreeNode::getId, Function.identity()));

        TreeNode root = idMappedNode.get(rootId);
        if (root == null) throw new IllegalArgumentException("nodes list not contains root node");

        for (TreeNode node : nodes) {
            idMappedNode.get(node.getParentId()).addChild(node);
        }

        return root;
    }

    /**
     * 将一组不包含跟节点的节点，根据父节点ID添加到父节点的子节点集合中，并返回父节点对象。
     * <p>
     * <b>注意：</b>
     * <li>调用该方法会生成一个id为root，父节点为N/A的根节点</li>
     * <li>调用此方法后参数nodes的children和level会被重置</li>
     * <li>如果参数nodes中的节点存在不能追溯到root的路径，则返回的root节点也不能向下获取到这些节点</li>
     * </p>
     *
     * @param nodes 包含了root节点的节点集合
     * @return root节点
     * @see com.jeremytech.utils.tree.TreeUtils#buildTreeWithRoot(Collection, String)
     */
    public static TreeNode buildTreeWithoutRoot(Collection<TreeNode> nodes) {
        if (nodes == null) throw new IllegalArgumentException("nodes list must be not null");
        TreeNode root = new TreeNode(TMP_ROOT_ID, TMP_ROOT_PARENT, 1);
        nodes.add(root);
        return buildTreeWithRoot(nodes, TMP_ROOT_ID);
    }

    /**
     * 查找指定父节点ID下的所有子孙节点
     *
     * @param allNodes         所有节点的Set集合，所有节点的子节点属性可以不设置，调用此函数会重新设置
     * @param parentId         父节点ID
     * @param isContainsParent allNodes中是否包含ID为parentId的节点
     * @return parentId下的所有子孙节点，其中不包括parentId的节点
     * @see com.jeremytech.utils.tree.TreeUtils#expandChildrenToList(TreeNode)
     */
    public static Set<TreeNode> findAllChildren(Set<TreeNode> allNodes, String parentId, boolean isContainsParent) {
        if (allNodes == null) throw new IllegalArgumentException("allNodes must be not null");
        if (StringValueUtils.isBlank(parentId))
            throw new IllegalArgumentException("parentId must not be null nor empty");
        if (allNodes.isEmpty()) return new HashSet<>();

        TreeNode parentNode;
        if (isContainsParent) {
            parentNode = buildTreeWithRoot(allNodes, parentId);
        } else {
            parentNode = buildTreeWithoutRoot(allNodes);
        }

        return expandChildrenToList(parentNode);
    }

    /**
     * 将指定节点的所有子孙节点展开为Set集合
     *
     * @param node 待展开的父节点
     * @return 所有子孙节点的Set集合，其中不包括node节点
     * @throws IllegalArgumentException node不能为空
     */
    public static Set<TreeNode> expandChildrenToList(TreeNode node) {
        if (node == null) throw new IllegalArgumentException("node must be not null");

        Set<TreeNode> allChildren = new HashSet<>();

        Set<TreeNode> children = node.getChildren();
        if (children != null && !children.isEmpty()) {
            for (TreeNode child : children) {
                allChildren.add(child);
                allChildren.addAll(expandChildrenToList(child));
            }
        }

        return allChildren;
    }

    /**
     * 根据指定节点ID向上查询至Root的节点列表，如果某个节点的父节点ID在allNodes中不存在则搜索停止
     *
     * @param allNodes       所有节点的结合
     * @param childId        指定节点ID
     * @param isContainsRoot allNodes是否包含Root节点
     * @param rootId         根节点ID，若isContainsRoot为false，则可传null，默认值为root
     * @return 包含childId节点并向上查询到root的所有节点集合
     */
    public static Set<TreeNode> findNodesToRoot(Set<TreeNode> allNodes, String childId, boolean isContainsRoot, String rootId) {
        if (allNodes == null) throw new IllegalArgumentException("allNodes must be not null");
        if (StringValueUtils.isBlank(childId)) throw new IllegalArgumentException("invalid childId");
        if (allNodes.isEmpty())
            throw new IllegalArgumentException("empty allNodes. The childId node must not reach to root");

        if (isContainsRoot) {
            return findNodesToRoot(allNodes, childId, rootId);
        } else {
            TreeNode root = new TreeNode(TMP_ROOT_ID, TMP_ROOT_PARENT, 1);
            allNodes.add(root);
            return findNodesToRoot(allNodes, childId, TMP_ROOT_ID);
        }
    }

    /**
     * 根据指定节点ID向上查询至Root的节点列表，如果某个节点的父节点ID在allNodes中不存在则搜索停止
     *
     * @param allNodes 包含root节点的所有节点
     * @param childId  指定节点ID
     * @param rootId   根节点ID
     * @return 包含childId节点并向上查询到root的所有节点集合
     */
    public static Set<TreeNode> findNodesToRoot(Set<TreeNode> allNodes, String childId, String rootId) {
        if (allNodes == null) throw new IllegalArgumentException("allNodes must be not null");
        if (StringValueUtils.isBlank(childId)) throw new IllegalArgumentException("invalid childId");
        if (allNodes.isEmpty())
            throw new IllegalArgumentException("empty allNodes. The childId node must not reach to root");

        Set<TreeNode> nodeSet = new HashSet<>();
        Map<String, TreeNode> mappedTreeNode = allNodes.stream()
                .collect(Collectors.toMap(TreeNode::getId, Function.identity()));

        TreeNode node = mappedTreeNode.get(childId);
        while (node != null) {
            nodeSet.add(node);
            if (rootId.equals(node.getId())) {
                break; // reach to root
            }
            node = mappedTreeNode.get(node.getParentId());
        }
        return nodeSet;
    }
}

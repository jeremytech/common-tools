package com.jeremytech.test.utils.tree;

import com.jeremytech.utils.tree.modle.TreeNode;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;

public class TreeUtilsTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(TreeUtilsTests.class);

    @Test
    public void overrideFunctionsTest() {
        TreeNode a = new TreeNode("3", "2", 3);
        TreeNode b = new TreeNode("2", "1", 2);
        Assert.assertTrue("hashCode()函数重写不满足要求", a.hashCode() == b.hashCode());
        LOGGER.info("[OK] TreeNode#hashCode()函数重写满足要求");
        Assert.assertTrue("equals()函数重写不满足要求", a.equals(b));
        LOGGER.info("[OK] TreeNode#equals()函数重写满足要求");

        b.addChild(new TreeNode("5", "4", 3));
        Assert.assertTrue("TreeNode#hashCode()函数重写不满足要求，添加子节点不会影响", a.hashCode() == b.hashCode());
        LOGGER.info("[OK] TreeNode#hashCode()函数重写满足要求，添加子节点会影响");
        Assert.assertTrue("equals()函数重写不满足要求，添加子节点不会影响", a.equals(b));
        LOGGER.info("[OK] TreeNode#equals()函数重写满足要求，添加子节点会影响");

        Set<TreeNode> sets = new HashSet<>();
        sets.add(a);
        sets.add(b);
        Assert.assertTrue("add元素进集合不满足条件", sets.size() == 1);
        LOGGER.info("[OK] 集合add TreeNode元素进集合满足预期");
        for (TreeNode node : sets) {
            Assert.assertTrue("后面的ADD不应该覆盖第一次ADD的元素", node.getChildren().size() == 0);
        }

        sets.clear();
        sets.add(b);
        sets.add(a);
        for (TreeNode node : sets) {
            Assert.assertTrue("后面的ADD不应该覆盖第一次ADD的元素", node.getChildren().size() == 1);
        }
        LOGGER.info("[OK] 集合add TreeNode元素的顺序变更后符合预期");
    }
}

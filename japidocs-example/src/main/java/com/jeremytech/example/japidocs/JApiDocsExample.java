package com.jeremytech.example.japidocs;

import io.github.yedaxia.apidocs.Docs;
import io.github.yedaxia.apidocs.DocsConfig;

/**
 * jApiDocs简单示例代码
 * 入门文档：<url>https://japidocs.agilestudio.cn/#/?id=getting-started</url>
 */
public class JApiDocsExample {

    public static void main(String[] args) {
        DocsConfig config = new DocsConfig();
        config.setProjectPath("G:\\DX\\01Code\\DX_ERP_SERVER_Git\\erp-system\\erp-biz\\erp-plancount\\erp-plancount-impl"); // root project path
        config.setProjectName("PlanCount"); // project name
        config.setApiVersion("V1.0");       // api version
        config.setDocsPath("G:\\DX\\01Code\\DX_ERP_SERVER_Git\\japidocs"); // api docs target path
        config.setAutoGenerate(Boolean.TRUE);  // auto generate
        Docs.buildHtmlDocs(config); // execute to generate
    }
}

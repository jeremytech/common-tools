package com.jeremytech.util.audit.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jeremytech.util.audit.dao.entity.AuditLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * AuditLog Mapper
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
@Mapper
public interface IAuditLogMapper extends BaseMapper<AuditLog> {
}

package com.jeremytech.util.audit.dao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jeremytech.util.audit.dao.api.IAuditLogDao;
import com.jeremytech.util.audit.dao.entity.AuditLog;
import com.jeremytech.util.audit.dao.mapper.IAuditLogMapper;

/**
 * AuditLog Dao Implement
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
public class AuditLogDao extends ServiceImpl<IAuditLogMapper, AuditLog> implements IAuditLogDao {
}

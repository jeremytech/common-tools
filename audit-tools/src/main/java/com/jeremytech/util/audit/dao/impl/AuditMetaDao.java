package com.jeremytech.util.audit.dao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jeremytech.util.audit.dao.api.IAuditMetaDao;
import com.jeremytech.util.audit.dao.entity.AuditMeta;
import com.jeremytech.util.audit.dao.mapper.IAuditMetaMapper;

/**
 * AuditMeta Dao Implement
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
public class AuditMetaDao extends ServiceImpl<IAuditMetaMapper, AuditMeta> implements IAuditMetaDao {
}

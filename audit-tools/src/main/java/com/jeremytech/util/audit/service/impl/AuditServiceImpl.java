package com.jeremytech.util.audit.service.impl;

import com.jeremytech.util.audit.dao.entity.AuditInfo;
import com.jeremytech.util.audit.model.AuditStatus;
import com.jeremytech.util.audit.service.AuditServiceFactory;
import com.jeremytech.util.audit.service.api.IAuditService;
import com.jeremytech.util.audit.vo.AuditInfoVo;

/**
 * 审核操作的接口实现
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
public class AuditServiceImpl extends AuditAbstractService {

    @Override
    public AuditInfoVo commitAudit(AuditInfo auditInfo) {
        IAuditService service = AuditServiceFactory.getService(auditInfo.getAuditType());
        if (service != null) {
            return service.commitAudit(auditInfo);
        }
        return null;
    }

    @Override
    public AuditInfoVo dealAudit(int auditId, AuditStatus status) {
        AuditInfo auditInfo = auditInfoDao.getById(auditId);
        if (auditInfo == null) return null;

        IAuditService service = AuditServiceFactory.getService(auditInfo.getAuditType());
        if (service != null) {
            return service.dealAudit(auditId, status);
        }
        return null;
    }
}

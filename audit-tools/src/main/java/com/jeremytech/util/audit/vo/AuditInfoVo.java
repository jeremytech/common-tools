package com.jeremytech.util.audit.vo;

import com.jeremytech.util.audit.model.AuditStatus;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * 审核信息视图层模型
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
@Data
public class AuditInfoVo {

    /**
     * 审核记录的自然ID，数据库自增，从0开始
     */
    private int auditId;

    /**
     * 审核记录的标题
     */
    private String auditName;

    /**
     * 审核记录的描述
     */
    private String auditDesc;

    /**
     * 审核类型
     */
    private int auditType;

    /**
     * 处理进度，表示当前已经到了第几步。 从1开始计数
     */
    private int processing;

    /**
     * 审核处理所需的总步数，不包括待审核和审核完成
     */
    private int totalStep;

    /**
     * 审核状态
     */
    private AuditStatus status;

    /**
     * 审核创建时间
     */
    private LocalDateTime createTime;

    /**
     * 审核完成时间
     */
    private LocalDateTime finishTime;

    /**
     * 审核提交者ID
     */
    private int creatorId;

    /**
     * 审核提交者姓名
     */
    private String creatorName;

    /**
     * 审核内容，以JSONString方式表示
     */
    private String auditContent;

    /**
     * 审核过程描述，key为数字，对应1到totalStep
     *
     * @see com.jeremytech.util.audit.dao.entity.AuditInfo#totalStep
     */
    private Map<Integer, List<AuditMetaVo>> metaMaps;
}

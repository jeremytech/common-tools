package com.jeremytech.util.audit.dao.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jeremytech.util.audit.dao.api.IAuditInfoDao;
import com.jeremytech.util.audit.dao.entity.AuditInfo;
import com.jeremytech.util.audit.dao.mapper.IAuditInfoMapper;

/**
 * AuditInfo Dao Implement
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
public class AuditInfoDao extends ServiceImpl<IAuditInfoMapper, AuditInfo> implements IAuditInfoDao {
}

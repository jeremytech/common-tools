package com.jeremytech.util.audit.vo;

import com.jeremytech.util.audit.model.AuditStatus;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 审核元信息视图层模型
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
@Data
public class AuditMetaVo {

    /**
     * 自然ID，数据库自增长主键
     */
    private int metaId;

    /**
     * 所属审核信息ID
     *
     * @see com.jeremytech.util.audit.dao.entity.AuditInfo#auditId
     */
    private int auditId;

    /**
     * 当前流程在审核中所处的第几步，基数从1开始，不包含待审核和审核完成
     *
     * @see com.jeremytech.util.audit.dao.entity.AuditInfo#processing
     * @see com.jeremytech.util.audit.dao.entity.AuditInfo#totalStep
     */
    private int step;

    /**
     * 当前节点审核状态
     */
    private AuditStatus auditStatus;

    /**
     * 审核时间
     */
    private LocalDateTime auditTime;

    /**
     * 审核者用户ID
     */
    private int auditorId;

    /**
     * 审核者用户姓名
     */
    private String auditorName;

    /**
     * 审核元信息内容，以JSONString方式呈现
     */
    private String metaContent;
}

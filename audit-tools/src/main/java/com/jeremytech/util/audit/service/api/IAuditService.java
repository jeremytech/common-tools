package com.jeremytech.util.audit.service.api;

import com.jeremytech.util.audit.dao.entity.AuditInfo;
import com.jeremytech.util.audit.dao.entity.AuditMeta;
import com.jeremytech.util.audit.model.AuditStatus;
import com.jeremytech.util.audit.vo.AuditInfoVo;
import com.jeremytech.util.audit.vo.AuditMetaVo;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 审核操作服务接口
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
public interface IAuditService {

    /**
     * 将AuditMeta转换成AuditMetaVo
     *
     * @param meta
     * @return
     */
    default AuditMetaVo metaToVo(AuditMeta meta) {
        AuditMetaVo vo = new AuditMetaVo();
        if (meta != null) {
            vo.setAuditId(meta.getAuditId());
            vo.setAuditorId(meta.getAuditorId());
            vo.setAuditorName(meta.getAuditorName());
            vo.setAuditStatus(meta.getStatus());
            vo.setAuditTime(meta.getAuditTime());
            vo.setMetaId(meta.getMetaId());
            vo.setStep(meta.getStep());
            vo.setMetaContent(meta.getMetaContent());
        }
        return vo;
    }

    /**
     * 将AuditInfo & List<AuditMeta>转换成AuditInfoVo
     *
     * @param auditInfo
     * @param metaList
     * @return
     */
    default AuditInfoVo infoToVo(AuditInfo auditInfo, List<AuditMeta> metaList) {
        AuditInfoVo vo = new AuditInfoVo();
        if (auditInfo != null) {
            vo.setAuditId(auditInfo.getAuditId());
            vo.setAuditName(auditInfo.getAuditName());
            vo.setAuditDesc(auditInfo.getAuditDesc());
            vo.setAuditType(auditInfo.getAuditType());
            vo.setProcessing(auditInfo.getProcessing());
            vo.setTotalStep(auditInfo.getTotalStep());
            vo.setStatus(auditInfo.getStatus());
            vo.setCreateTime(auditInfo.getCreateTime());
            vo.setFinishTime(auditInfo.getFinishTime());
            vo.setCreatorId(auditInfo.getCreatorId());
            vo.setCreatorName(auditInfo.getCreatorName());
            vo.setAuditDesc(auditInfo.getAuditDesc());
        }
        if (metaList != null && !metaList.isEmpty()) {
            List<AuditMetaVo> metaVoList = metaList.stream()
                    .map(x -> metaToVo(x))
                    .collect(Collectors.toList());
            Map<Integer, List<AuditMetaVo>> metaMaps = metaVoList.stream()
                    .collect(Collectors.groupingBy(AuditMetaVo::getStep));
            vo.setMetaMaps(metaMaps);
        } else {
            vo.setMetaMaps(Collections.EMPTY_MAP);
        }
        return vo;
    }

    /**
     * 提交审核
     *
     * @param auditInfo 审核信息
     * @return
     */
    AuditInfoVo commitAudit(AuditInfo auditInfo);

    /**
     * 根据审核ID查询审核详情
     *
     * @param auditId
     * @return
     */
    AuditInfoVo queryByAuditId(int auditId);

    /**
     * 根据多个ID查询审核列表
     *
     * @param ids
     * @return
     */
    List<AuditInfoVo> queryList(List<Integer> ids);

    /**
     * 处理审核
     *
     * @param auditId
     * @param metaId
     * @param status
     * @return
     */
    AuditInfoVo dealAudit(int auditId, int metaId, AuditStatus status);

    /**
     * 根据审核ID删除审核
     *
     * @param auditId
     * @return
     */
    boolean deleteByAuditId(int auditId);

    /**
     * 将指定的审核回退到指定的处理步骤
     *
     * @param auditId
     * @param toStep
     * @return
     */
    AuditInfoVo rollback(int auditId, int toStep);
}

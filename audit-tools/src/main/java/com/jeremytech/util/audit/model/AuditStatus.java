package com.jeremytech.util.audit.model;

/**
 * 审核状态
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
public enum AuditStatus {

    NEW(0),
    PROCESSING(10),
    ACCEPTED(90),
    REFUSED(91),
    DELETED(93);

    AuditStatus(int id) {
        this.id = id;
        switch (this.id) {
            case 0:
                this.desc = "待审核";
                return;
            case 10:
                this.desc = "审核中";
                return;
            case 90:
                this.desc = "审核通过";
                return;
            case 91:
                this.desc = "审核拒绝";
                return;
            case 93:
                this.desc = "已删除";
                return;
        }
    }

    private int id;

    private String desc;

    /**
     * 获取当前状态的ID
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * 获取当前状态的描述
     *
     * @return
     */
    public String getDesc() {
        return desc;
    }

}

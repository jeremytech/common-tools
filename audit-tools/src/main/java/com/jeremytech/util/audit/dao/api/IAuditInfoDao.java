package com.jeremytech.util.audit.dao.api;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jeremytech.util.audit.dao.entity.AuditInfo;

/**
 * AuditInfo Dao
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
public interface IAuditInfoDao extends IService<AuditInfo> {
}

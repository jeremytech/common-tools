package com.jeremytech.util.audit.model;

/**
 * 审核类型枚举
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
public enum AuditType {

    AUDIT_TYPE_EXAMPLE(1);

    AuditType(int type) {
        this.type = type;
    }

    private int type;

    public int getType() {
        return type;
    }

    /**
     * 根据类型获取相应的枚举
     *
     * @param type
     * @return
     */
    public static final AuditType typeOf(int type) {
        return AUDIT_TYPE_EXAMPLE;
    }
}

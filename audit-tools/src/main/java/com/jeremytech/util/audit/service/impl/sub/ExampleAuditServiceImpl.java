package com.jeremytech.util.audit.service.impl.sub;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.jeremytech.util.audit.dao.entity.AuditInfo;
import com.jeremytech.util.audit.dao.entity.AuditLog;
import com.jeremytech.util.audit.dao.entity.AuditMeta;
import com.jeremytech.util.audit.model.AuditStatus;
import com.jeremytech.util.audit.service.impl.AuditAbstractService;
import com.jeremytech.util.audit.vo.AuditInfoVo;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 示例审核服务实现
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
public class ExampleAuditServiceImpl extends AuditAbstractService {

    @Override
    public AuditInfoVo commitAudit(AuditInfo auditInfo) {
        auditInfoDao.save(auditInfo);
        saveAuditMeta(auditInfo);
        return queryByAuditId(auditInfo.getAuditId());
    }

    @Override
    public AuditInfoVo dealAudit(int auditId, int metaId, AuditStatus status) {
        AuditInfo auditInfo = auditInfoDao.getById(auditId);
        if (auditInfo == null) return null;

        int processing = auditInfo.getProcessing();
        AuditMeta auditMeta = auditMetaDao.getById(metaId);
        if (auditMeta == null || auditMeta.getStep() != processing) return null;

        AuditStatus metaStatus = auditMeta.getStatus();
        if (metaStatus.getId() >= 90) return null; // 终止状态的审核不能再审

        // 审核元信息
        auditMeta.setStatus(status);
        auditMeta.setAuditorId(100); // 根据实际情况填写
        auditMeta.setAuditorName("jeremy"); // 根据实际情况填写
        LocalDateTime now = LocalDateTime.now();
        auditMeta.setAuditTime(now);
        auditMetaDao.updateById(auditMeta);

        // 记录元信息操作日志
        AuditLog metaLog = new AuditLog();
        metaLog.setAuditId(auditId);
        metaLog.setMetaId(metaId);
        metaLog.setFromStatus(metaStatus);
        metaLog.setToStatus(status);
        metaLog.setAuditorId(100);
        metaLog.setAuditorName("jeremy");
        metaLog.setModifyTime(now);
        metaLog.setMemo("update by jeremy");
        auditLogDao.save(metaLog);

        LambdaQueryWrapper<AuditMeta> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(AuditMeta::getAuditId, auditId);
        wrapper.eq(AuditMeta::getStep, processing);
        wrapper.in(AuditMeta::getStatus, new int[]{
                AuditStatus.NEW.getId(),
                AuditStatus.PROCESSING.getId()
        });
        int metaCount = auditMetaDao.count(wrapper);
        if (metaCount == 0 && processing == auditInfo.getTotalStep()) {
            auditInfo.setFinishTime(now);
            auditInfo.setStatus(status);
            auditInfoDao.updateById(auditInfo);

            AuditLog infoLog = new AuditLog();
            infoLog.setAuditId(auditId);
            infoLog.setFromStatus(AuditStatus.PROCESSING);
            infoLog.setToStatus(status);
            infoLog.setAuditorId(100);
            infoLog.setAuditorName("jeremy");
            infoLog.setModifyTime(now);
            infoLog.setMemo("update because of last meta audited by jeremy");
            auditLogDao.save(infoLog);
        }

        return queryByAuditId(auditId);
    }

    /**
     * 保存审核元信息
     *
     * @param auditInfo
     * @return
     */
    private boolean saveAuditMeta(AuditInfo auditInfo) {
        boolean saveRet = saveStepOne(auditInfo);
        saveRet &= saveStepTwo(auditInfo);
        return saveRet;
    }

    /**
     * 保存审核第一步的元信息
     *
     * @param auditInfo
     * @return
     */
    private boolean saveStepOne(AuditInfo auditInfo) {
        AuditMeta step1 = new AuditMeta();
        step1.setAuditId(auditInfo.getAuditId());
        step1.setStep(1);
        step1.setStatus(AuditStatus.NEW);
        AuditMetaContent content = new AuditMetaContent();
        content.setTitle("计划部长审核");
        content.setDepartId(10);
        step1.setMetaContent(JSONObject.toJSONString(content));
        return auditMetaDao.save(step1);
    }

    /**
     * 保存第二步的审核元信息
     *
     * @param auditInfo
     * @return
     */
    private boolean saveStepTwo(AuditInfo auditInfo) {
        AuditMeta step2 = new AuditMeta();
        step2.setAuditId(auditInfo.getAuditId());
        step2.setStep(2);
        step2.setStatus(AuditStatus.NEW);
        AuditMetaContent content = new AuditMetaContent();
        content.setTitle("项目经理审核");
        content.setDepartId(13);
        step2.setMetaContent(JSONObject.toJSONString(content));
        return auditMetaDao.save(step2);
    }

    /**
     * 审核元信息内容描述对象，可根据实际修改
     */
    @Data
    private class AuditMetaContent {
        private String title;
        private int departId;
    }
}

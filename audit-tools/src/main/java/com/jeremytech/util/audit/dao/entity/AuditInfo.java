package com.jeremytech.util.audit.dao.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.jeremytech.util.audit.model.AuditStatus;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * 审核主体信息
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
@Data
@TableName(value = "AUDIT_INFO")
public class AuditInfo implements Serializable {

    /**
     * 审核记录的自然ID，数据库自增，从0开始
     */
    @TableId(value = "id", type = IdType.AUTO)
    private int auditId;

    /**
     * 审核记录的标题
     */
    @TableField(value = "name")
    private String auditName;

    /**
     * 审核记录的描述
     */
    @TableField(value = "desc")
    private String auditDesc;

    /**
     * 审核类型
     */
    @TableField(value = "type")
    private int auditType;

    /**
     * 处理进度，表示当前已经到了第几步。 从1开始计数
     */
    @TableField(value = "processing")
    private int processing;

    /**
     * 审核处理所需的总步数，不包括待审核和审核完成
     */
    @TableField(value = "total_step")
    private int totalStep;

    /**
     * 审核状态
     */
    @TableField(value = "status")
    private AuditStatus status;

    /**
     * 审核创建时间
     */
    @TableField(value = "create_time")
    private LocalDateTime createTime;

    /**
     * 审核完成时间
     */
    @TableField(value = "finish_time")
    private LocalDateTime finishTime;

    /**
     * 审核提交者ID
     */
    @TableField(value = "creator_id")
    private int creatorId;

    /**
     * 审核提交者姓名
     */
    @TableField(exist = false)
    private String creatorName;

    /**
     * 审核内容，以JSONString方式表示
     */
    @TableField(value = "content")
    private String auditContent;

    /**
     * 逻辑删除标记，0-正常；1-已删除
     */
    @TableLogic(value = "0", delval = "1")
    private char delFlag;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuditInfo auditInfo = (AuditInfo) o;
        return auditId == auditInfo.auditId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(auditId);
    }
}

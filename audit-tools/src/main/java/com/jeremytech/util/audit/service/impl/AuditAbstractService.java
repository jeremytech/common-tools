package com.jeremytech.util.audit.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.jeremytech.util.audit.dao.api.IAuditInfoDao;
import com.jeremytech.util.audit.dao.api.IAuditLogDao;
import com.jeremytech.util.audit.dao.api.IAuditMetaDao;
import com.jeremytech.util.audit.dao.entity.AuditInfo;
import com.jeremytech.util.audit.dao.entity.AuditLog;
import com.jeremytech.util.audit.dao.entity.AuditMeta;
import com.jeremytech.util.audit.model.AuditStatus;
import com.jeremytech.util.audit.service.api.IAuditService;
import com.jeremytech.util.audit.vo.AuditInfoVo;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 次抽象类包含公共的审核操作实现
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
public abstract class AuditAbstractService implements IAuditService {

    protected IAuditInfoDao auditInfoDao;

    protected IAuditMetaDao auditMetaDao;

    protected IAuditLogDao auditLogDao;

    @Override
    public final AuditInfoVo queryByAuditId(int auditId) {
        AuditInfo auditInfo = auditInfoDao.getById(auditId);
        if (auditInfo == null) return null;

        List<AuditMeta> metaList = queryMetaListByAuditId(auditId);
        return infoToVo(auditInfo, metaList);
    }

    @Override
    public final List<AuditInfoVo> queryList(List<Integer> ids) {
        LambdaQueryWrapper<AuditInfo> wrapper = Wrappers.lambdaQuery();
        wrapper.in(AuditInfo::getAuditId, ids);
        List<AuditInfo> auditInfoList = auditInfoDao.list(wrapper);

        if (auditInfoList == null || auditInfoList.isEmpty()) {
            return Collections.EMPTY_LIST;
        }

        List<AuditInfoVo> auditInfoVoList = auditInfoList.stream()
                .map(x -> {
                    List<AuditMeta> metaList = queryMetaListByAuditId(x.getAuditId());
                    return infoToVo(x, metaList);
                })
                .collect(Collectors.toList());
        return auditInfoVoList;
    }

    @Override
    public final boolean deleteByAuditId(int auditId) {
        AuditInfo auditInfo = auditInfoDao.getById(auditId);
        if (auditInfo == null) return false;

        boolean infoDelRet = auditInfoDao.removeById(auditId);
        if (!infoDelRet) return infoDelRet;

        LocalDateTime now = LocalDateTime.now();
        // 简单填写属性，后面按需修改
        AuditLog auditLog = new AuditLog();
        auditLog.setAuditId(auditId);
        auditLog.setFromStatus(auditInfo.getStatus());
        auditLog.setToStatus(AuditStatus.DELETED);
        auditLog.setModifyTime(now);
        auditLogDao.save(auditLog);

        List<AuditMeta> metaList = queryMetaListByAuditId(auditId);
        if (metaList != null && !metaList.isEmpty()) {
            metaList.forEach(x -> {
                auditMetaDao.removeById(x.getMetaId());

                AuditLog metaLog = new AuditLog();
                metaLog.setAuditId(x.getAuditId());
                metaLog.setMetaId(x.getMetaId());
                metaLog.setFromStatus(x.getStatus());
                metaLog.setToStatus(AuditStatus.DELETED);
                metaLog.setModifyTime(now);
                metaLog.setMemo("Delete because of AuditInfo deleted");
                auditLogDao.save(metaLog);
            });
        }

        return true;
    }

    @Override
    public final AuditInfoVo rollback(int auditId, int toStep) {
        AuditInfo auditInfo = auditInfoDao.getById(auditId);
        if (auditInfo == null) return null;

        // 处于起始状态或终止状态的审核不能回滚
        if (auditInfo.getStatus().getId() == 0 || auditInfo.getStatus().getId() >= 90) return null;
        // 回滚到的阶段只能在如右的范围  --->   [1,processing)
        if (toStep <= 0 || toStep >= auditInfo.getProcessing()) return null;

        LambdaQueryWrapper<AuditMeta> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(AuditMeta::getAuditId, auditId);
        wrapper.ge(AuditMeta::getStep, toStep);
        wrapper.le(AuditMeta::getStep, auditInfo.getProcessing());
        List<AuditMeta> metaList = auditMetaDao.list(wrapper);
        LocalDateTime now = LocalDateTime.now();
        if (metaList != null && !metaList.isEmpty()) {
            metaList.forEach(x -> {
                AuditLog metaLog = new AuditLog();
                metaLog.setAuditId(x.getAuditId());
                metaLog.setMetaId(x.getMetaId());
                metaLog.setFromStatus(x.getStatus());
                metaLog.setToStatus(AuditStatus.NEW);
                metaLog.setModifyTime(now);
                metaLog.setMemo("Rollback because of AuditInfo rollback");
                auditLogDao.save(metaLog);

                x.setAuditorId(0);
                x.setAuditorName("");
                x.setAuditTime(null);
                x.setStatus(AuditStatus.NEW);
                auditMetaDao.updateById(x);
            });
        }

        auditInfo.setProcessing(toStep);
        auditInfoDao.updateById(auditInfo);

        AuditLog auditLog = new AuditLog();
        auditLog.setAuditId(auditId);
        auditLog.setFromStatus(auditInfo.getStatus());
        auditLog.setToStatus(auditInfo.getStatus());
        auditLog.setModifyTime(now);
        auditLog.setMemo("Rollback to step ".concat(String.valueOf(toStep)));
        auditLogDao.save(auditLog);

        return queryByAuditId(auditId);
    }

    /**
     * 根据审核ID查询所有的审核元信息列表
     *
     * @param auditId
     * @return
     */
    protected List<AuditMeta> queryMetaListByAuditId(int auditId) {
        LambdaQueryWrapper<AuditMeta> wrapper = Wrappers.lambdaQuery();
        wrapper.eq(AuditMeta::getAuditId, auditId);
        return auditMetaDao.list(wrapper);
    }
}

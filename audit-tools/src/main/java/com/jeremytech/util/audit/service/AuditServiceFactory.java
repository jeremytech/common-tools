package com.jeremytech.util.audit.service;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ReferenceConfig;
import com.alibaba.dubbo.config.RegistryConfig;
import com.jeremytech.util.audit.model.AuditType;
import com.jeremytech.util.audit.service.api.IAuditService;
import com.jeremytech.util.audit.service.impl.sub.ExampleAuditServiceImpl;

/**
 * 审核服务工厂类，提供根据审核类型获取审核服务实例的工厂方法
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
public class AuditServiceFactory {

    /**
     * 根据审核类型获取服务实例
     *
     * @param auditType
     * @return
     */
    public static IAuditService getService(int auditType) {
        AuditType type = AuditType.typeOf(auditType);
        // 此处实现需要考虑单例模式，本示例暂不处理，使用时根据实际情况修改
        switch (type) {
            case AUDIT_TYPE_EXAMPLE:
                return new ExampleAuditServiceImpl();
        }
        return null;
    }

    /**
     * 通过审核类型从ZooKeeper+Dubbo的RPC服务中获取IAuditService的服务实例<br/>
     * 参考：<url>https://blog.csdn.net/qq_20009015/article/details/106985441</url>
     *
     * @param auditType
     * @return
     */
    public static IAuditService getServiceFromZkDubbo(int auditType) {
        // [必要设置] 创建服务引用对象实例
        ReferenceConfig<IAuditService> refConfig = new ReferenceConfig<>();
        // [必要设置] 设置应用信息
        refConfig.setApplication(new ApplicationConfig("dubbo"));
        // [必要设置] 设置服务注册中心 zookeeper://127.0.0.1:2181
        refConfig.setRegistry(new RegistryConfig("zookeeper://127.0.0.1:2181"));
        // [必要设置] 设置服务接口
        refConfig.setInterface(IAuditService.class);
        // [必要设置] 设置服务分组与版本
        refConfig.setVersion("AUDIT_TYPE." + auditType);

        // 设置服务超时时间，按需设置
        refConfig.setTimeout(5000);
        // 设置服务分组，如果没有可以不设置
        refConfig.setGroup("group1");
        // 设置为异步，如果没有必要可以不设置
        refConfig.setAsync(true);

        return refConfig.get();
    }
}

package com.jeremytech.util.audit.dao.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.jeremytech.util.audit.model.AuditStatus;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 审核每一步的元信息
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
@Data
@TableName(value = "AUDIT_META_INFO")
public class AuditMeta {

    /**
     * 自然ID，数据库自增长主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private int metaId;

    /**
     * 所属审核信息ID
     *
     * @see com.jeremytech.util.audit.dao.entity.AuditInfo#auditId
     */
    @TableField(value = "audit_id")
    private int auditId;

    /**
     * 当前流程在审核中所处的第几步，基数从1开始，不包含待审核和审核完成
     *
     * @see com.jeremytech.util.audit.dao.entity.AuditInfo#processing
     * @see com.jeremytech.util.audit.dao.entity.AuditInfo#totalStep
     */
    @TableField(value = "step")
    private int step;

    /**
     * 当前节点审核状态
     */
    @TableField(value = "status")
    private AuditStatus status;

    /**
     * 审核时间
     */
    @TableField(value = "audit_time")
    private LocalDateTime auditTime;

    /**
     * 审核者用户ID
     */
    @TableField(value = "auditor_id")
    private int auditorId;

    /**
     * 审核者用户姓名
     */
    @TableField(exist = false)
    private String auditorName;

    /**
     * 审核元信息内容，以JSONString方式呈现
     */
    @TableField(value = "content")
    private String metaContent;

    /**
     * 逻辑删除标记，0-正常；1-已删除
     */
    @TableLogic(value = "0", delval = "1")
    private char delFlag;
}

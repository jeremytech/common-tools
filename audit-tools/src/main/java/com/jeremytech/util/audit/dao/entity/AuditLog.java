package com.jeremytech.util.audit.dao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jeremytech.util.audit.model.AuditStatus;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 审核操作日志
 *
 * @author jeremy
 * @date 2021-01-18
 * @copyright JeremyTech
 * @since V1.0.0
 */
@Data
@TableName(value = "AUDIT_LOG")
public class AuditLog {

    /**
     * 审核信息ID
     *
     * @see com.jeremytech.util.audit.dao.entity.AuditInfo#auditId
     */
    @TableField(value = "audit_id")
    private int auditId;

    /**
     * 审核元信息ID
     *
     * @see com.jeremytech.util.audit.dao.entity.AuditMeta#metaId
     */
    @TableField(value = "meta_id")
    private int metaId;

    /**
     * 变更钱状态
     */
    @TableField(value = "from_status")
    private AuditStatus fromStatus;

    /**
     * 变更后状态
     */
    @TableField(value = "to_status")
    private AuditStatus toStatus;

    /**
     * 状态变更时间
     */
    @TableField(value = "modify_time")
    private LocalDateTime modifyTime;

    /**
     * 状态变更发起人用户ID
     */
    @TableField(value = "auditor_id")
    private int auditorId;

    /**
     * 状态变更发起人姓名
     */
    @TableField(exist = false)
    private String auditorName;

    /**
     * 备注信息
     */
    @TableField(value = "memo")
    private String memo;
}
